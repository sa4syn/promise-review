const fs = require('fs');

function getId(userName, userType = 0){
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            if(!userName || userName.length > 8) {
                reject({code : 322, message: 'invalid user name'});
            } else {
                let id = Math.ceil(Math.random() * 10000000000);
                resolve({userId: id,
                    'userName' : userName,
                    'userType': userType});
            }
        }, 1000);
    });
}

function promiseToReadJsonFile(fileName){
    return new Promise((resolve, reject) => {
        fs.readFile(fileName, 'utf-8', (err, fileContent) => {
            if(err){
                reject({'code' : 543,'message' : 'no such file'});
            } else{
                resolve(JSON.parse(fileContent));
            }
        })
    })
}

function promiseToWriteJsonFile(fileName, fileContent){
    return new Promise((resolve, reject) => {
        fs.writeFile(fileName, JSON.stringify(fileContent), 'utf-8', (err) => {
            if(err){
                reject({'code' : 343, 'message' : "can't add to cart"});
            } else {
                resolve();
            }
        })
    })
}

function addItemToCart(userId, itemArray){
    return promiseToReadJsonFile('./carts.json')
    .then((cart) => {
        if(!cart[userId]){
            cart[userId] = [];
        }
        cart[userId].push(...itemArray);
        return promiseToWriteJsonFile('./carts.json', cart)
    })
    .catch((err) => console.log(err));
}

function getMostExpnesiveItem(){
    return new Promise((resolve, reject) => {
        promiseToReadJsonFile('./price.json')
        .then((prices) => {
            if(!prices) resolve('');
            let mostExpensiveItem = Object.entries(prices).reduce((desiredProduct ,product) => {
                let price = product[1]['price'] - product[1]['discount'];
                if(price > desiredProduct['price']){
                    desiredProduct['id'] = product[0];
                    desiredProduct['price'] = price;
                }
                return desiredProduct;
            }, {id : '', price : 0})
            resolve(mostExpensiveItem);
        });
    })
}  

function getRandomItem(){
    return new Promise((resolve, reject) => {
        promiseToReadJsonFile('./price.json')
        .then((prices) => {
            if(!prices) resolve('');
            let randomIndex = Math.ceil(Math.random() * Object.entries(prices).length);
            let randomItem = Object.entries(prices)[randomIndex];
            resolve({id : randomItem[0], price : randomItem[1]['price'] - randomItem[1]['discount']});
        });
    })
}

function addRandomItem(userId){
    return getRandomItem()
    .then((mostExpensiveItem) => {
       promiseToReadJsonFile('./products.json')
       .then((productList) => {
           let id = mostExpensiveItem['id'];
           let product = [{'_id' : id, 'name' : productList[id]['name'], 'quantity' : Math.ceil(Math.random() * 10)}];
           //console.log(product);
           return addItemToCart(userId, product);
       })
   }) 
}

function addMostExpensiveItem(userId){
     return getMostExpnesiveItem()
     .then((mostExpensiveItem) => {
        promiseToReadJsonFile('./products.json')
        .then((productList) => {
            let id = mostExpensiveItem['id'];
            let product = [{'_id' : id, 'name' : productList[id]['name'], 'quantity' : 1}];
            //console.log(product);
            return addItemToCart(userId, product);
            //return product;
        })
    }) 
}

async function getProductDetails(){
    const productDetailsStream = fs.createWriteStream('productDetails.txt');
    const product = await promiseToReadJsonFile('./products.json');
    const prices = await promiseToReadJsonFile('./price.json');
    Object.entries(product).forEach(product => {
        productDetailsStream.write(`${product[1]['name']} ${prices[product[0]]['price'] - prices[product[0]]['discount']}\n`);
    });
    productDetailsStream.end();
}

module.exports = { getId, getProductDetails, addMostExpensiveItem, addRandomItem };