const { getId, getProductDetails, addMostExpensiveItem, addRandomItem } = require('../solution.js');

async function newUserOperations (userName){
    let newUser = await getId(userName);
    return addRandomItem(newUser['userId'])
    .then(() => addMostExpensiveItem(newUser['userId']));
}

newUserOperations('insh')
.then(() => newUserOperations('buddy'))
.catch((err) => console.log(err));

getProductDetails();
